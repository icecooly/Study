package org.study.algorithm;
import java.util.Arrays;

/**
 * 比赛对阵
 * @author icecooly
 *
 */
public class TestSnake {
	
	public static int[] computeNextRound(int round,int teamSize){
		int[] array=new int[teamSize];
		for(int i=0;i<array.length;i++){
			array[i]=i;
		}
		for(int r=0;r<round;r++){
			for(int i=1;i<teamSize;i++){
				array[i]++;
				if(array[i]==array.length){
					array[i]=1;
				}
			}
		}
		int[] ret;;
		if((teamSize%2)==0){
			ret=new int[teamSize];
		}else{
			ret=new int[teamSize-1];
		}
		
		for(int i=0;i<teamSize/2;i++){
			ret[i*2]=array[i];
			ret[i*2+1]=array[teamSize-1-i];
		}
		return ret;
	}
	public static void main(String[] args) {
		for(int i=1;i<20;i++){
			System.out.println(Arrays.toString(computeNextRound(i, 20)));
		}
	}
	//
	
}
