package org.study.java8;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

interface IFormula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}
@FunctionalInterface
interface Converter<F, T> {
    T convert(F from);
}

public class Formula {
	public static void main(String[] args) {
		IFormula formula = new IFormula() {
		    @Override
		    public double calculate(int a) {
		        return sqrt(a * 100);
		    }
		};
		System.out.println(formula.calculate(100));     // 100.0
		System.out.println(formula.sqrt(16));     
		//
		List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
		Collections.sort(names, (String a, String b) -> b.compareTo(a));
		System.out.println(names);
		//
		Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
		Integer converted = converter.convert("123");
		System.out.println(converted);    // 123
		//
		Converter<String, Integer> converter1 = Integer::valueOf;
		Integer converted1 = converter1.convert("123");
		System.out.println(converted1);   // 123
	}
}

