package org.study.java8;

import java.util.Objects;

public class NullTest {
	
	private String obj;
	
	public void process(String obj){
		this.obj=Objects.requireNonNull(obj,"obj is null");
		System.out.println(this.obj);
	}
	//
	public static void main(String[] args) {
		NullTest test=new NullTest();
		test.process("NullTest");
		test.process(null);
	}
}
