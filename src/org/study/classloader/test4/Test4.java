package org.study.classloader.test4;

import java.io.File;

import org.study.classloader.FileSystemClassLoader;

public class Test4 implements Runnable{

	private Team team;
	
	public void init() throws InterruptedException{
//		team=new Team();
//		while(true){
//			System.out.println("fk="+team.getFK());
//			Thread.sleep(1000);
//		}
	}
	public static void main(String[] args) throws InterruptedException {
		Test4 test=new Test4();
		Thread t=new Thread(test);
		t.start();
		test.init();
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		while(true){
			File f=new File("/tmp/com/classloader/test4/Team.class");
			if(f.exists()){
				String classDataRootPath = "/tmp"; 
			    FileSystemClassLoader loader = new FileSystemClassLoader(classDataRootPath); 
			   String className = "com.classloader.test4.Team";
			    try { 
			        Class<?> class1 = loader.loadClass(className); 
			        team=(Team) class1.newInstance();
			        System.out.println("fk="+team.getFK()+","+Team.class.getClassLoader());
			    } catch (Exception e) {
			        e.printStackTrace(); 
			    }
			    return;
			}
			try {
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
