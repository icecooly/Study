package org.study.classloader;
/**
 * 
 * @author icecooly
 *
 */
public class ClassLoaderTree { 

    public static void main(String[] args) {
        ClassLoader loader = ClassLoaderTree.class.getClassLoader(); 
        while (loader!=null) { 
            System.out.println(loader.toString()); 
            loader = loader.getParent(); 
        }
        System.out.println(Object.class.getClassLoader());
        
    } 
 }