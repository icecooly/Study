package org.study.classloader.test1;
/**
 * 
 * @author icecooly
 *
 */
public class SubClass extends SuperClass{

	static{
		System.out.println("SubClass init!");
	}
	
	public SubClass(){
		System.out.println("SubClass xxx!");
	}
}
