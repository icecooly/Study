package org.study.classloader.thread;

/**
 * 
 * @author icecooly
 *
 */
public class TestThread implements Runnable{

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getId()+","+Thread.currentThread().getContextClassLoader());
		Thread.currentThread().setContextClassLoader(new ClassLoader1());
	}
	
	public static void main(String[] args) {
		for(int i=0;i<2;i++){
			Thread t=new Thread(new TestThread());
			t.start();
		}
	}

}
