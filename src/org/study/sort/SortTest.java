package org.study.sort;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class SortTest {

	public static void main(String[] args) {
		List<NBAPlayer> list=new ArrayList<NBAPlayer>();
		list.add(new NBAPlayer(1, 100));
		list.add(new NBAPlayer(2, 50));
		list.add(new NBAPlayer(3, 150));
		list.add(new NBAPlayer(4, 300));
		list.add(new NBAPlayer(5, 10));
		list.add(new NBAPlayer(6, 200));
		Collections.sort(list,new Comparator<NBAPlayer>() {
			@Override
			public int compare(NBAPlayer o1,NBAPlayer o2) {
				return o2.getMarketPrice()-o1.getMarketPrice();
			}
		});
		
		for(NBAPlayer e:list){
			System.out.println(e);
		}
	}
}
