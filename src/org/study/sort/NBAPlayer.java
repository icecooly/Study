package org.study.sort;
import java.io.Serializable;

/**
 * 
 * @author icecooly
 *
 */
public class NBAPlayer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int playerId;
	
	private int marketPrice;
	
	public NBAPlayer(int playerId,int marketPrice){
		this.playerId=playerId;
		this.marketPrice=marketPrice;
	}
	//

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(int marketPrice) {
		this.marketPrice = marketPrice;
	}

	@Override
	public String toString() {
		return "NBAPlayer [playerId=" + playerId + ", marketPrice="
				+ marketPrice + "]";
	}
	
}
