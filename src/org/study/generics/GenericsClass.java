package org.study.generics;


/**
 * 
 * @author icecooly
 *
 * @param <T>
 */
public class GenericsClass<T> implements SimpleList<T>{
	/**当前长度*/
	@SuppressWarnings("unused")
	private int size;
	/**数据*/
	@SuppressWarnings("unused")
	private Object[] data;
	
	public GenericsClass(){
		data=new Object[10];
		
	}
	
	public GenericsClass(int size){
		if(size<0){
	         throw new IllegalArgumentException("Illegal Capacity:"+
	            		size);
		}
		data=new Object[size];
	}
	
	@Override
	public void add(T e) {
		
	}

	@Override
	public void get(int index) {
		
	}

}
