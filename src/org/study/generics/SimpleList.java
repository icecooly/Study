package org.study.generics;


public interface SimpleList<T> {

	void add(T e);
	
	void get(int index);
	
}
