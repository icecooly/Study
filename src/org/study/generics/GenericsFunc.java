package org.study.generics;
import java.util.ArrayList;
import java.util.List;

/**
 * 可变参数的范型
 * @author icecooly
 *
 */
public class GenericsFunc{
	
	public static <T> List<T> makeList(@SuppressWarnings("unchecked") T... args){
		List<T> result=new ArrayList<T>();
		for(T e:args){
			result.add(e);
		}
		return result;
	}
	public static void main(String[] args) {
		List<String> ls=makeList("A","B","C");
		System.out.println(ls);
		
		List<Integer> list=makeList(1,2,3);
		System.out.println(list);
	}

}
