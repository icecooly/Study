package org.study.annotation;
import java.lang.reflect.Method;


public class TestAnnotation {

	public static void trackUserCases(Class<?> c1){
		for(Method m:c1.getDeclaredMethods()){
			UserCase uc=m.getAnnotation(UserCase.class);
			if(uc!=null){
				System.out.println("Found User Case:"+uc.id()+" "+
						uc.description());
			}
		}
	}
	public static void main(String[] args) {
		trackUserCases(PasswordUtils.class);
	}
}
