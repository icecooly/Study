package org.study.annotation;

public class PasswordUtils {

	@UserCase(id=47,description="password must contain at least one numeric")
	public boolean validatePassword(String password){
		return (password.matches("\\w*\\d\\w*"));
	}
}
