package org.study.https;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
 
public class HttpsClient{
 
 
   private void testIt() throws Exception{
 
      String https_url = "https://localhost:8443/";
      URL url;
      try {
    	 System.setProperty("javax.net.ssl.keyStore", "C:\\Users\\sky.du\\.keystore");
         System.setProperty("javax.net.ssl.keyStorePassword", "111111");
         System.setProperty("javax.net.ssl.trustStore","C:\\Users\\sky.du\\.keystore");     
         System.setProperty("javax.net.ssl.trustStorePassword","111111");
         System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
         HostnameVerifier hv = new HostnameVerifier() {
             public boolean verify(String urlHostName, SSLSession session) {
             return urlHostName.equals(session.getPeerHost());
             }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
         url = new URL(https_url);
	     //
	   //创建SSLContext对象，并使用我们指定的信任管理器初始化
	     TrustManager[] tm = {new MyX509TrustManager ()};
	     SSLContext sslContext = SSLContext.getInstance("SSL","SunJSSE");
	     sslContext.init(null, tm, new java.security.SecureRandom());

	     //从上述SSLContext对象中得到SSLSocketFactory对象
	     SSLSocketFactory ssf = sslContext.getSocketFactory();
	     //
	     HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	     con.setSSLSocketFactory(ssf);
	     
	     //dumpl all cert info
	     print_https_cert(con);
 
	     //dump all the content
	     print_content(con);
 
      } catch (MalformedURLException e) {
	     e.printStackTrace();
      } catch (IOException e) {
	     e.printStackTrace();
      }
 
   }
 
   private void print_https_cert(HttpsURLConnection con){
 
    if(con!=null){
 
      try {
 
	System.out.println("Response Code : " + con.getResponseCode());
	System.out.println("Cipher Suite : " + con.getCipherSuite());
	System.out.println("\n");
 
	Certificate[] certs = con.getServerCertificates();
	for(Certificate cert : certs){
	   System.out.println("Cert Type : " + cert.getType());
	   System.out.println("Cert Hash Code : " + cert.hashCode());
	   System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
	   System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
	   System.out.println("\n");
	}
 
	} catch (SSLPeerUnverifiedException e) {
		e.printStackTrace();
	} catch (IOException e){
		e.printStackTrace();
	}
 
     }
 
   }
 
   private void print_content(HttpsURLConnection con){
	if(con!=null){
 
	try {
 
	   System.out.println("****** Content of the URL ********");			
	   BufferedReader br = 
		new BufferedReader(
			new InputStreamReader(con.getInputStream()));
 
	   String input;
 
	   while ((input = br.readLine()) != null){
	      System.out.println(input);
	   }
	   br.close();
 
	} catch (IOException e) {
	   e.printStackTrace();
	}
 
       }
 
   }
   public static void main(String[] args) throws Exception
   {
        new HttpsClient().testIt();
   }
 
}