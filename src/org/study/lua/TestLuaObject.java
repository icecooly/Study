package org.study.lua;

import java.util.List;

public class TestLuaObject {

	public int a;
	
	public float b;
	
	public String c;
	
	public List<Integer> d;
	//
	public TestLuaObject(int a,float b,String c,List<Integer> d){
		this.a=a;
		this.b=b;
		this.c=c;
		this.d=d;
	}
	//
	@Override
	public String toString() {
		return "TestLuaObject [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d
				+ "]";
	}
	
}
