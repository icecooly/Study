package org.study.lua;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.keplerproject.luajava.LuaObject;
import org.keplerproject.luajava.LuaState;
import org.keplerproject.luajava.LuaStateFactory;

/**
 * 比赛入口
 * @author sky.du
 *
 */
public class TestLua{
	//
	public LuaState luaState;
	//
	private static class  InstanceHolder{
		private static TestLua instance=new TestLua();
	}
	public static TestLua get(){
		return InstanceHolder.instance;
	}
	//
	private TestLua(){
		System.out.println(System.getProperty("java.library.path"));
		luaState = LuaStateFactory.newLuaState();
		luaState.openLibs();
		luaState.LdoFile("res/TestLua.lua");
	}
	//
	private boolean getBooleanResult(LuaState luaState){
		luaState.setField(LuaState.LUA_GLOBALSINDEX, "result");
		LuaObject result = luaState.getLuaObject("result");
		return 	result.getBoolean();
	}
	//
	private synchronized boolean test(TestLuaObject testLuaObj) throws Exception{
		luaState.getField(LuaState.LUA_GLOBALSINDEX, "testLua");  
		luaState.pushJavaObject(testLuaObj);
		luaState.call(1,0); 
		//
		luaState.getField(LuaState.LUA_GLOBALSINDEX, "checkValid");  
		luaState.call(0,1);
		boolean result=getBooleanResult(luaState);
		return result;
	}
	//
	public static void main(String[] args) throws Exception {
		System.out.println(System.getProperty("java.library.path"));
		final TestLua testLua=TestLua.get();
		BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();    
		ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 6, 1, TimeUnit.DAYS, queue);    
		for (int i = 0; i < 1; i++) {    
		    executor.execute(new Runnable() {
				@Override
				public void run() {
					TestLuaObject testLuaObject=new TestLuaObject(1,2.5f,"ce测",Arrays.asList(1,2,3));
					try {
						testLua.test(testLuaObject);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			});
		}    
		executor.shutdown(); 
	}
	
}
