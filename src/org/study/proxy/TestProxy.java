package org.study.proxy;

import org.study.proxy.action.HelloWorldAction;

/**
 * 
 * @author icecooly
 *
 */
public class TestProxy {
	
	public static void main(String[] args) throws Exception {
		HelloWorldAction action=ActionFactroy.get().createRemote(HelloWorldAction.class);
		action.print("Hello World",new int[]{1,2,3});
	}
}
