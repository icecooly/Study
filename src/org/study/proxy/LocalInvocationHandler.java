package org.study.proxy;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 
 * @author icecooly
 *
 */
public class LocalInvocationHandler implements InvocationHandler{
	
	private static LocalInvocationHandler instance;
	
	public static LocalInvocationHandler get(){
		if(instance==null){
			instance=new LocalInvocationHandler();
		}
		return instance;
	}
	private LocalInvocationHandler(){
		
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		System.out.println("invoke catch proxy:"+
			proxy.getClass().getSimpleName()+",method:"+method.getName());
		ActionFactroy.get().invoke(proxy,method,args);
		return null;
	}

}
