package org.study.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ActionFactroy {
	
	private static ActionFactroy instance;
	
	public static ActionFactroy get(){
		if(instance==null){
			instance=new ActionFactroy();
		}
		return instance;
	}
	private Map<Class<?>,Object> actionMap;
	
	private ActionFactroy(){
		actionMap=new HashMap<Class<?>, Object>();
	}
	@SuppressWarnings("unchecked")
	public <T> T createLocal(Class<T> action) throws Exception{
		Object o=actionMap.get(action); 
		if(o!=null){
			return (T)o;
		}
		String className=action.getPackage().getName()+".impl."+action.getSimpleName()+"Impl";
		Class<?> a=Class.forName(className);
		o=a.newInstance();
		actionMap.put(action, o);
		return (T)o;
	}
	@SuppressWarnings("unchecked")
	public <T> T createRemote(Class<T> action) throws Exception{
		return (T) Proxy.newProxyInstance(
				action.getClassLoader(),  
				new Class[]{action},
				LocalInvocationHandler.get());  
	}
	public void invoke(Object o, Method method, Object[] args) throws Exception {
		System.out.println("ActionFactroy invoke proxy:"+
				o.getClass().getSimpleName()+",method:"+method.getName()+",targetClass:"+
				method.getDeclaringClass()+","+method.getDeclaringClass().getSimpleName());
		Object obj=createLocal(method.getDeclaringClass());
		method.invoke(obj, args);
	}
}
