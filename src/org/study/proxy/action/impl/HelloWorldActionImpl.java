package org.study.proxy.action.impl;

import java.util.Arrays;

import org.study.proxy.action.HelloWorldAction;

public class HelloWorldActionImpl implements HelloWorldAction{

	@Override
	public void print(String str,int[] intArray) {
		System.out.println(str+","+Arrays.toString(intArray));
	}
}
