package org.study.exception;

public class TestException {

	public void testError() throws Exception{
		throw new Error();
	}
	
	public static void main(String[] args){
//		TestException te=new TestException();
//		try {
//			te.testError();
//		} catch (Exception e) {
//			System.out.println("is exception");
//			e.printStackTrace();
//		}
		//
		try {
			System.out.println("start");
			throw new IllegalAccessException("IllegalAccessException");
		} catch (Exception e) {
			System.out.println("catch "+e.getMessage());
		}finally{
			System.out.println("finally");
		}
	}
}
