package org.study.jmx; 
 public class ServerMonitor implements ServerMonitorMBean { 
    private ServerImpl server; 
    public ServerMonitor(ServerImpl server){ 
        this.server = server; 
    }
    @Override
    public int getVersion(){ 
        return server.getVersion();
    }
    @Override
    public void setVersion(int version){ 
        server.setVersion(version);  
    } 
 } 