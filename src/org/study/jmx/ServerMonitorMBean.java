package org.study.jmx;

public interface ServerMonitorMBean {

	int getVersion();

	void setVersion(int version);
}
