package org.study.jmx; 
 public class ServerImpl { 
	 
    private int version=1; 
    
    public ServerImpl() { 
      
    }

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "ServerImpl [version=" + version + "]";
	}
    
 } 