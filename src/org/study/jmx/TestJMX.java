 package org.study.jmx; 
 import javax.management.Attribute;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
 public class TestJMX { 
    private static ObjectName objectName ; 
    private static MBeanServer mBeanServer; 
    
    private static void init() throws Exception{ 
        ServerImpl serverImpl = new ServerImpl(); 
        ServerMonitor serverMonitor = new ServerMonitor(serverImpl); 
        mBeanServer = MBeanServerFactory.createMBeanServer(); 
        objectName = new ObjectName("objectName:id=ServerMonitor1"); 
        mBeanServer.registerMBean(serverMonitor,objectName);  
    } 
    private static void manage() throws Exception{
    	//get version
        int version = (Integer) mBeanServer.getAttribute(objectName,"Version"); 
        System.out.println(version);
        
        //set version
        Attribute attr=new Attribute("Version", 2);
        mBeanServer.setAttribute(objectName,attr); 
        
      //get version
        version = (Integer) mBeanServer.getAttribute(objectName,"Version"); 
        System.out.println(version);
    }
    public static void main(String[] args) throws Exception{ 
        init(); 
        manage();               
    } 
 } 