package org.study.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * 
 * @author icecooly
 *
 */
public class FileUtil {
	
	//
	public static String getContent(File file) throws IOException{
		return new String(Files.readAllBytes(file.toPath()),StandardCharsets.UTF_8);
	}
	//
	public static void main(String[] args) throws IOException {
		String content=FileUtil.getContent(new File("test"));
		System.out.println(content);
	}
}
