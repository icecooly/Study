package org.study.util;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;
  
public class RSATest {  
  
     private static String module = "5m9m14XH3oqLJ8bNGw9e4rGpXpcktv9MSkHSVFVMjHbfv+SJ5v0ubqQxa5YjLN4vc49z7SVju8s0X4gZ6AzZTn06jzWOgyPRV54Q4I0DCYadWW4Ze3e+BOtwgVU1Og3qHKn8vygoj40J6U85Z/PTJu3hN1m75Zr195ju7g9v4Hk=";     
     private static String exponentString = "AQAB";     
     private static String privateExponentBase64 = "vmaYHEbPAgOJvaEXQl+t8DQKFT1fudEysTy31LTyXjGu6XiltXXHUuZaa2IPyHgBz0Nd7znwsW/S44iql0Fen1kzKioEL3svANui63O3o5xdDeExVM6zOf1wUUh/oldovPweChyoAdMtUzgvCbJk1sYDJf++Nr0FeNW1RB1XG30=";     
       //
        public static byte[] encrypt(String input,String moduleBase64,String exponentBase64) {     
            try {     
                byte[] modulusBytes = Base64Helper.decode(moduleBase64);
                byte[] exponentBytes = Base64Helper.decode(exponentBase64);     
                BigInteger modulus = new BigInteger(1, modulusBytes);     
                BigInteger exponent = new BigInteger(1, exponentBytes); 
                System.err.println("modulus:"+modulus.toString());
                System.err.println("exponentBytes:"+exponent.toString());
                RSAPublicKeySpec rsaPubKey = new RSAPublicKeySpec(modulus, exponent);     
                KeyFactory fact = KeyFactory.getInstance("RSA");     
                PublicKey pubKey = fact.generatePublic(rsaPubKey);     
        
                Cipher cipher = Cipher.getInstance("RSA");     
                cipher.init(Cipher.ENCRYPT_MODE, pubKey);     
        
                byte[] cipherData = cipher.doFinal(input.getBytes());     
                return cipherData;     
            } catch (Exception e) {     
                e.printStackTrace();     
            }     
            return null;     
        
        }     
        
        public static byte[] Dencrypt(byte[] encrypted,String moduleBase64,String exponentBase64) {     
            try {     
                byte[] exponentBytes = Base64Helper.decode(exponentBase64);     
                byte[] modBytes = Base64Helper.decode(moduleBase64);     
        
                BigInteger modulus = new BigInteger(1, modBytes);     
                BigInteger exponent = new BigInteger(1, exponentBytes);     
        
                System.err.println("modulus:"+modulus.toString());
                System.err.println("exponentBytes:"+exponent.toString());
                
                KeyFactory factory = KeyFactory.getInstance("RSA");     
                Cipher cipher = Cipher.getInstance("RSA");     
        
                RSAPrivateKeySpec privSpec = new RSAPrivateKeySpec(modulus, exponent);     
                PrivateKey privKey = factory.generatePrivate(privSpec);     
                cipher.init(Cipher.DECRYPT_MODE, privKey);     
                byte[] decrypted = cipher.doFinal(encrypted);     
                return decrypted;     
            } catch (Exception e) {     
                e.printStackTrace();     
            }     
            return null;    
            
        }
        /**     
         * @param args     
         */     
        public static void main(String[] args) {    
        	module="OTMxNjI5NTIyNjI1MzQzMTQ5NjcwNjM1NDg5ODA4NDQ3NTI5OTUwNTM0NDUzODkwNzUzMjM4ODY1NDA0ODE0MTA3NzIxMjEzNjk2MDMwMzY4MDk1MzU2ODIyMTE4NjMzMDE0NzQ4MDEyNTM4NzMyNTQ0NDcxNTA1NDY1OTY3ODYyMzg2MzgzNjQxNDMzMTMyNTk0NzUzMjEwMzUyMzQzNDM1NDczNDYzMDM2Mzg3MDAwMjg2MzM0NjkwMjk3OTI0NDYwOTA4ODI1MzYyMTYwODQwMTk2Njg1OTQ5NjQ3Nzk3NDk1MDI4MzE2Mjc0MDIzMDk4NDEyNDc4NDg1MjcwMjQ0NzM4ODEzMTMzMDMyMzc2NDA0MjM3OTMwODM4MTI4MTA1ODAyODA1NjQ2NTgyOTkxMDE=";
        	
        	byte[] en = encrypt("xxxjiiii",module,exponentString); 
            String encodeBase64=Base64Helper.encode(en);
            byte[] encode=Base64Helper.decode(encodeBase64);
            
            
            privateExponentBase64="ODU0MTk4NjY2NjI0MzAxODQyNjc5ODM3MTM5MDYwMjE5NjAyMjgxNTc1NTI3MzI0OTUxNzM5MDY5ODcxNjAzNTE3NjAwMjUyMjM5MTA4NjY4NjczNDIwOTkwMjk3MjE2MTk2Mjg5MjQyMzEyNzc2NTc4MDc0ODcwMTI2MDM1ODA5NTU0ODc0MjM5MTg4ODA4NDIzMDA4Njg4MTkyODg2MzgyMTg5MTQ1MDAxNjQ1NTc1Njk5ODIxNzA4MDI4NjA2NjEyMTM0MTE3Njc4MTYzNzQ2NTI2OTk2MTMyMTU0NDY1MDQ3NzkyOTAyNzA0OTM5NTU4MjA2NTk0NjgzODEyMjIyMjc2MjQyMjIyMjQ0ODM0NDQ4Njg5MzQyMjk0Mjk1ODc5MzI1MTI4NjkwNzQxODAzMTM=";
            byte[] jiemi=Dencrypt(encode,module,privateExponentBase64);
            System.out.println(new String(jiemi));
//            
//            System.out.println(Base64Helper.encode(en));     
//            byte[] enTest = null;     
//               
//           enTest = Base64Helper.decode(encryptString);     
//                
//            System.out.println(enTest.length);     
//            System.out.println(en.length);     
//               
//            System.out.println(new String(Dencrypt(enTest)));     
        }     
}  