package org.study.reflect;

public class SimpleClass extends SimpleParentClass implements SimpleInterface{

	
	public void test(SimpleInterface c){
		System.out.println(c.getClass().getName());
	}
	
	public static void main(String[] args) {
		SimpleClass sc=new SimpleClass();
		sc.test((SimpleInterface)sc);
	}

}
