package org.study.finallyreturn;

public class TestFinallyReturn {

	public Data test(){
		Data data=new Data();
		data.setData(100);
		try {
			process(data);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return data;
		}finally{
			data.setData(0);
		}
	}
	
	private void process(Data data){
		data.setData(data.getData()+10);
	}
	
	public static void main(String[] args) {
		TestFinallyReturn test=new TestFinallyReturn();
		Data data=test.test();
		System.out.println(data);
	}
}
