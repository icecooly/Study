package org.study.hex;
import java.io.UnsupportedEncodingException;


public class Hex {
   
	public static String bytesToHexString(byte[] src){  
	    StringBuilder stringBuilder = new StringBuilder("");  
	    if (src == null || src.length <= 0) {  
	        return null;  
	    }  
	    for (int i = 0; i < src.length; i++) {  
	        int v = src[i] & 0xFF;  
	        String hv = Integer.toHexString(v);  
	        if (hv.length() < 2) {  
	            stringBuilder.append(0);  
	        }  
	        stringBuilder.append(hv);  
	    }  
	    return stringBuilder.toString();  
	}  
	/** 
	 * Convert hex string to byte[] 
	 * @param hexString the hex string 
	 * @return byte[] 
	 */  
	public static byte[] hexStringToBytes(String hexString) {  
	    if (hexString == null || hexString.equals("")) {  
	        return null;  
	    }  
	    hexString = hexString.toUpperCase();  
	    int length = hexString.length() / 2;  
	    char[] hexChars = hexString.toCharArray();  
	    byte[] d = new byte[length];  
	    for (int i = 0; i < length; i++) {  
	        int pos = i * 2;  
	        d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));  
	    }  
	    return d;  
	}  
	/** 
	 * Convert char to byte 
	 * @param c char 
	 * @return byte 
	 */  
	 private static byte charToByte(char c) {  
	    return (byte) "0123456789ABCDEF".indexOf(c);  
	}  
	//将指定byte数组以16进制的形式打印到控制台  
    public static void printHexString(byte[] b) {    
       for (byte e:b) {   
    	   printHexString(e);  
       }   
    }
  //将指定byte数组以16进制的形式打印到控制台  
    public static void printHexString(Byte b) {    
    	String hex = Integer.toHexString(b& 0xFF);   
    	if (hex.length() == 1) {   
    		hex='0'+hex;   
    	}   
    	System.out.print(hex.toUpperCase() );
    }
    
	public static void main(String[] args) throws UnsupportedEncodingException {
		String hexString="00 00 00 A8 AC ED 00 05 73 72 01 00 26 63 6F 6D 2E 66 74 78 67 61 6D 65 2E 73 68 61 72 70 2E 72 70 63 73 65 72 76 65 72 2E 52 50 43 52 65 71 75 65 73 74 78 70 00 00 00 00 00 00 00 01 00 00 00 01 46 D0 F7 CD 92 70 74 00 10 67 65 74 41 6C 6C 50 6C 61 79 65 72 4C 69 73 74 75 72 01 00 13 5B 4C 6A 61 76 61 2E 6C 61 6E 67 2E 53 74 72 69 6E 67 3B 78 70 00 00 00 00 70 70 70 74 00 2E 63 6F 6D 2E 66 74 78 67 61 6D 65 2E 66 62 61 6C 6C 2E 64 61 74 61 2E 62 69 7A 2E 61 63 74 69 6F 6E 2E 50 6C 61 79 65 72 41 63 74 69 6F 6E";
		byte[] b=hexStringToBytes(hexString.replaceAll(" ", ""));
		System.out.println(new String(b));
		//
	}
}
