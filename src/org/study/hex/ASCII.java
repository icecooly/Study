package org.study.hex;

public class ASCII {

	//ASCII码表:http://baike.baidu.cn/view/15482.htm
	
	public static void main(String[] args) {
		//ASCII码表
		for(byte i=-128;i<128;i++){
			System.out.println(i+":"+(char)i);
			if(i==127){
				break;
			}
		}
		
		int a=0x30;
		System.out.println(a+":"+(char)a);
	}
}
