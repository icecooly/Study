package org.study.atomic;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicIntegerTest {
	private final AtomicLong counter = new AtomicLong(0);

	public AtomicLong getCounter() {
		return counter;
	}

	public void test() {
		for (int i = 0; i < 10; i++) {
			new Thread(new Runnable() {
				public void run() {
					counter.getAndIncrement();
				}
			}).start();
		}
	}

	public static void main(String[] args) {
		AtomicIntegerTest obj = new AtomicIntegerTest();
		obj.test();
		System.out.println(obj.getCounter());// 期望输出500，可有时输出498、496
	}
}