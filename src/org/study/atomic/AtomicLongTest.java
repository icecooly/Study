package org.study.atomic;
import java.util.concurrent.atomic.AtomicLong;


public class AtomicLongTest {
	public static void main(String[] args) {
		AtomicLong al=new AtomicLong();
		System.out.println(al);
		al.incrementAndGet();
		System.out.println(al);
		
		if(al.get()>=1){
			System.out.println(">=1");
		}else{
			System.out.println("<1");
		}
		
		al.set(0);
		System.out.println(al);
		
	}
}
