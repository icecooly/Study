package org.study.varargs;

public class NewVarArgs {

	static void printArray(Object... args){
		for(Object e:args){
			System.out.print(e+" ");
		}
		System.out.println();
	}
	public static void main(String[] args) {
		printArray(1,2,3);
		printArray("1","2","3");
		printArray();
	}
}
