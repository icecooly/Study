package org.study.mongo;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class TestMongo {
	private DBCollection connection;
	//
	public TestMongo(){
	}
	
	public DBCollection getConnection(String host,String dbName,String connectionName) throws Exception{
		MongoClient mongoClient = new MongoClient("localhost");
		DB db=mongoClient.getDB(dbName);
		connection = db.getCollection(connectionName);
		return connection;
	}
	
	public <T> void add(T o){
		DBObject dbObject = (DBObject) JSON.parse(com.alibaba.fastjson.JSON.toJSONString(o));
		connection.insert(dbObject);
	}
	
}
