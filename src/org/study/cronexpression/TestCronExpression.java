package org.study.cronexpression;
import java.text.ParseException;
import java.util.Date;
/**
 * 
 * @author icecooly
 *
 */
public class TestCronExpression {

	public static void main(String[] args) throws ParseException {
			String cronExpression="0 30 15 * * ?";
			CronExpression cron=new CronExpression(cronExpression);
			Date nextTime=cron.getNextValidTimeAfter(new Date());
			System.out.println(nextTime);
		
	}
}
