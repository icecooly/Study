package org.study.finalize;

public class TestFinalize {
	private boolean checkOut;
	
	TestFinalize(boolean checkOut){
		this.checkOut=checkOut;
	}
	void checkIn(){
		this.checkOut=false;
	}
	protected void finalize() {
		if(checkOut){
			System.out.println("Error checkOut");
		}
	}
	public static void main(String[] args) throws InterruptedException {
		TestFinalize novel=new TestFinalize(true);
		novel.checkIn();
		
		new TestFinalize(true);
		
		System.gc();
		while(true){
			Thread.sleep(10000);
		}
	}
}
