package org.study.random;
import java.util.HashMap;
import java.util.Map;



public class MyRandom {

	private long d;
	
	private final static long a=16807;
	
	private  final static long m=2147483647L;
	
	private  final static long c=5001;
	
//	private final static long a=1103515245L;
//	
//	private  final static long m=4294967296L;
//	
//	private  final static long c=12345;
	//
	public MyRandom(int seed){
		this.d=seed;
	}
	public MyRandom(){
		this.d=System.currentTimeMillis()/1000;
	}
	
	public long next(){
		return  d=(a*d+c)%m;
	}
	
	public int next(int n){
		return  (int) (next()%n);
	}
	
	public static void main(String[] args) {
		int maxNum=1000;
		MyRandom MyRand=new MyRandom();
		Map<Integer,Integer> map=new HashMap<Integer,Integer>();
		for(int i=0;i<maxNum;i++){
			map.put(i, 0);
		}
		for(int i=0;i<100000000;i++){
			int ret=MyRand.next(maxNum);
			if(ret<0){
				System.err.println("ret<0"+ret);
			}
			int count=map.get(ret);
			map.put(ret, count+1);
		}
		for(int i=0;i<maxNum;i++){
			System.out.println("map["+i+"]="+map.get(i));
		}
//		MyRandom myRand=new MyRandom(100);
//		for(int i=0;i<1000;i++){
//			System.out.println(myRand.next(100));
//		}
	}
	
}
