package org.study.serializable.fastjson;

import org.study.serializable.SerializableBean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class TestFastJson{

    public static void main(String args[]) throws Exception {
    	int testCount=100000000;
    	SerializableBean bean=new SerializableBean();
    	//
    	byte compressBytes[]=JSON.toJSONBytes(bean,SerializerFeature.WriteClassName);
    	System.out.println(compressBytes.length);
    	//
    	long startTime=System.currentTimeMillis();
        for(int i=0;i<testCount;i++){
        	compressBytes=JSON.toJSONBytes(bean,SerializerFeature.WriteClassName);
        }
        long endTime=System.currentTimeMillis();
        System.out.println("encode waste:"+(endTime-startTime)+"ms");
        //
        startTime=System.currentTimeMillis();
        for(int i=0;i<testCount;i++){
        	 JSON.parseObject(compressBytes,SerializableBean.class);
        }
        endTime=System.currentTimeMillis();
        System.out.println("decode waste:"+(endTime-startTime)+"ms");
       
    }
}
