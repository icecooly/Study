package org.study.serializable.spearal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.spearal.DefaultSpearalFactory;
import org.spearal.SpearalDecoder;
import org.spearal.SpearalEncoder;
import org.spearal.SpearalFactory;
import org.study.serializable.SerializableBean;


public class TestSpearal{

    public static void main(String args[]) throws Exception {
    	int testCount=100000000;
    	SerializableBean bean=new SerializableBean();
    	SpearalFactory factory = new DefaultSpearalFactory();
    	//
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	SpearalEncoder encoder = factory.newEncoder(baos);
    	encoder.writeAny(bean);
    	byte[] compressBytes = baos.toByteArray();
    	System.out.println(compressBytes.length);
    	//
    	long startTime=System.currentTimeMillis();
        for(int i=0;i<testCount;i++){
        	baos = new ByteArrayOutputStream();
        	encoder = factory.newEncoder(baos);
        	encoder.writeAny(bean);
        	compressBytes = baos.toByteArray();
        }
        long endTime=System.currentTimeMillis();
        System.out.println("encode waste:"+(endTime-startTime)+"ms");
        //
        startTime=System.currentTimeMillis();
        for(int i=0;i<testCount;i++){
        	 ByteArrayInputStream bais = new ByteArrayInputStream(compressBytes);
             SpearalDecoder decoder = factory.newDecoder(bais);
             decoder.readAny(SerializableBean.class);
        }
        endTime=System.currentTimeMillis();
        System.out.println("decode waste:"+(endTime-startTime)+"ms");
    }
}
