package org.study.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author icecooly
 *
 */
public class SerializableBean implements Serializable{


	/**序列化id*/
	private static final long serialVersionUID = 1L;
	//
	/**球队id*/
	public int teamId;
	/**帐号id*/
	public String accountId;
	/**球队名*/
	public String teamName;
	/**合作方id*/
	public int serviceId;
	/**服务器id*/
	public String serverId;
	/**等级*/
	public int grade;
	/**经验*/
	public int exp;
	/**饭卡*/
	public int fk;
	/**饭票*/
	public int fp;
	/**友情点*/
	public int friendPoint;
	/**最大替补席球员数量*/
	public int maxPlayerNum;
	/**行动力*/
	public int actionPoint;
	/**队徽*/
	public int teamLogo;
	/**综合实力*/
	public int cap;
	/**战绩：总胜场*/
	public int winCount;
	/**战绩：总负场*/
	public int loseCount;
	/**执教的教练*/
	public int currCoachId;
	/**雇佣的教练列表*/
	public List<Integer> coachList;
	/**当前使用的阵型*/
	public int currLineup;
	/**上半场默认战术*/
	public int currTactics1;
	/**下半场默认战术*/
	public int currTactics2;
	/**联盟id*/
	public int leagueId;
	/**联盟名称*/
	public String leagueName;
	/**联盟角色*/
	public int[] leagueRole;
	/**联盟贡献*/
	public int contributePoint;
	/**总联盟贡献*/
	public int allContributePoint;
	/**行动力自动恢复起始时间*/
	public Date actionPointTime;
	/**胜点*/
	public int winPoint;
	/**荣誉点*/
	public int honorPoint;
	/**奖杯(杯赛奖励)*/
	public int cup;
	/**球队等级工资帽*/
	public int gradeSalaryCap;
	/**联盟工资帽*/
	public int leagueSalaryCap;
	/**vip等级*/
	public int vipGrade;
	/**总充值人民币*/
	public int totalChargeNum;
	/**连续登录天数*/
	public int continuousLoginDays;
	/**最后登录时间*/
	public Date lastLoginTime;
	//
	public SerializableBean(){
		coachList=new ArrayList<Integer>();
		leagueRole=new int[5];
	}
	
}