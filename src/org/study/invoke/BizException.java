package org.study.invoke;

/**
 * 
 * @author icecooly
 *
 */
public class BizException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	//
	protected int code;
	public BizException() {
		
	}
	//
	public BizException(int code) {
		super();
		this.code=code;
	}
	//
	public BizException(int code,String msg){
		super(msg);
		this.code=code;
	}
	//
	public BizException(int code,String msg,Throwable e){
		super(msg, e);
		this.code=code;
	}
	//
	public BizException(int code,Throwable e){
		super(e);
		this.code=code;
	}
	//
	public int getCode() {
		return code;
	}
	//
	@Override
	public String toString() {
		return "BizException [code=" + code + "]";
	}
	
}
