package org.study.invoke;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestInvoke {

	public int test(int a) throws Exception{
		if(a==0){
			throw new BizException(1,"a==0 exception");
		}
		return a;
	}
	//
	public static void main(String[] args){
		TestInvoke ti=new TestInvoke();
		Throwable exception=null;
		for(Method m:TestInvoke.class.getDeclaredMethods()){
			if(!Modifier.isStatic(m.getModifiers())){
				System.out.println(m);
				try {
					int ret=(int) m.invoke(ti, 0);
					System.out.println("ret:"+ret);
				}catch (InvocationTargetException e) {
					exception = e.getTargetException();
				} catch (Throwable e) {
					exception = e;
				}finally{
					if(exception instanceof BizException){
						System.out.println((BizException)exception);
					}
				}
				
			}
		}
	}
	
	
}
