//查看包类型
print(java);
print(java.lang);

//创建Java对象
var HashMap = Java.type("java.util.HashMap");
var mapDef = new HashMap();
var map100 = new HashMap(100);
print(Java.type("java.util.Map").Entry);

//JavaString测试
var StringCls = Java.type("java.lang.String");
var str = new StringCls("Hello");
str = str.toUpperCase();
print('Upper: ' + str);

//测试输出
Java.type("java.lang.System").out.println(10);
Java.type("java.lang.System").out["println(double)"](10.92929);

//日期
var Date = Java.type('java.util.Date');
var date = new Date();
print('year:' + date.year);
date.year += 1000;
print('new year:' + date.year);

//字符串处理
print("   hehe".trimLeft());			// hehe
print("hehe	".trimRight() + "he");   // hehehe


//简单函数的定义
function sqr(x) x * x;
print(sqr(3));	// 9

//Lamda表达式
var map = Array.prototype.map;
var names = ["john", "jerry", "bob"];
//调用匿名函数，输入参数是names，匿名函数自动遍历names的内容name，并计算其长度
var a = map.call(names, function(name) { return name.length() });
print('Lamda表达式：' + a);

//try...catch用法
try {
  iarr[10] = 5;
} 
catch (exp) {
  print('try...catch...用法：' + exp.message);
}

var imports = new JavaImporter(
		java.io,
		java.lang,
		org.study.script
		);
with (imports) {
	var file = new File(__FILE__);  //查找我在哪里？
	System.out.println('哪里: ' + file.getAbsolutePath());   //内容比较古怪
}

var imports = new JavaImporter(
	org.study.script
);
with (imports) {
	var server=new Server("testServer");
}
$.registerServer(server);
