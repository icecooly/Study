package org.study.script;

public class Server {

	private String name;
	//
	public Server(String name){
		this.name=name;
	}
	//
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Server [name=" + name + "]";
	}
	
}
