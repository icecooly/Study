package org.study.script;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleScriptContext;

public class Container {

	private static Container container;
	
	public static  Container get(){
		if(container==null){
			container=new Container();
		}
		return container;
	}
	private Server server;
	
	private void loadConfig() throws Exception{
		ScriptEngineManager engineManager = new ScriptEngineManager();  
		ScriptEngine engine = engineManager.getEngineByName("nashorn");  
	    //  
	    ScriptContext engineContext = new SimpleScriptContext();  
	    engine.setContext(engineContext);  
	    engine.put("$", this); 
	    
	    String fileName="newstart.js";
		String packageName=Container.class.getPackage().getName();
		fileName=packageName.replaceAll("\\.", File.separator)+File.separator+fileName;
		InputStream is=Container.class.getClassLoader().getResourceAsStream(fileName);
	    engine.eval(new InputStreamReader(is));
	}
	
	public void init() throws Exception{
		loadConfig();
	}
	
	public void start(){
		System.out.println(server);
	}
	
	public void registerServer(Server server){
		this.server=server;
		
	}
	//

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	@Override
	public String toString() {
		return "Container [server=" + server + "]";
	}
	
	public static void main(String[] args) throws Exception {
		Container.get().init();
		Container.get().start();
	}
	
}
