package org.study.script;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleScriptContext;

public class TestScript {
	
	public void runActivty() throws Exception{
		long startTime=System.currentTimeMillis();
		ScriptEngineManager engineManager = new ScriptEngineManager();  
		ScriptEngine engine = engineManager.getEngineByName("nashorn");  
	    //  
	    ScriptContext engineContext = new SimpleScriptContext();  
	    engine.setContext(engineContext);  
	    engine.put("$", this); 
	    
	    String fileName="activity1.js";
		String packageName=Container.class.getPackage().getName();
		fileName=packageName.replaceAll("\\.", File.separator)+File.separator+fileName;
		InputStream is=Container.class.getClassLoader().getResourceAsStream(fileName);
	    engine.eval(new InputStreamReader(is));
	    long endTime=System.currentTimeMillis();
	    System.out.println("waster"+(endTime-startTime)+"ms");
	}
	
	public void process(){
		System.out.println("process");
	}
	
	public static void main(String[] args) throws Exception {
		TestScript ta=new TestScript();
		while(true){
			ta.runActivty();
			Thread.sleep(5000);
		}
	}
}
