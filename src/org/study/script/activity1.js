 function sleep(n) {
    var start = new Date().getTime();
    while(true)  if(new Date().getTime()-start > n) break;
}
 
//查看包类型
print(java);
print(java.lang);

//创建Java对象
var HashMap = Java.type("java.util.HashMap");
var mapDef = new HashMap();
var map100 = new HashMap(100);
print(Java.type("java.util.Map").Entry);

//JavaString测试
var StringCls = Java.type("java.lang.String");
var str = new StringCls("Hello");
str = str.toUpperCase();
print('Upper: ' + str);

//测试输出
Java.type("java.lang.System").out.println(10);
Java.type("java.lang.System").out["println(double)"](10.92929);

//日期
var Date = Java.type('java.util.Date');
var date = new Date();
print('year:' + date.year);
date.year += 1000;
print('new year:' + date.year);

$.process();

sleep(5000);

//字符串处理
print("   hehe".trimLeft());			// hehe
print("hehe	".trimRight() + "he");   // hehehe
