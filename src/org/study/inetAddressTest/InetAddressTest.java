package org.study.inetAddressTest;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class InetAddressTest {

	public static void main(String[] args) throws UnknownHostException {
		
		InetAddress ia=InetAddress.getByName("183.16.159.43");
		System.out.println(ia.getHostAddress());
		System.out.println(ia.getHostName());
	}
}
